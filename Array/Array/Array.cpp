﻿#include <Windows.h>
#include <iostream>

using std::cout;
using std::endl;

int main()
{
    setlocale(LC_ALL, "RU");

    int i, j;
    int A=0;
    const int N=4; 
    int Array[N][N];
    int DayNow;

    //Получение сегодняшней даты
    SYSTEMTIME st;
    GetLocalTime(&st);
    DayNow = st.wDay;
    
    /* 1. В главном исполняемом файле(файл, в котором находится функция main)
    создайте двумерный массив размерности NxN и заполните его так, чтобы
    элемент с индексами i и j был равен i + j. 
    2. Выведите этот массив в консоль.*/
    cout << "Двумерный массив при N равном "<< N <<" :" << endl;
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
           Array[i][j] = (i + j);
           cout << Array[i][j];
        }
        cout << endl;
    }

    /* 3. Выведите сумму элементов в строке массива, индекс которой равен
    остатку деления текущего числа календаря на N(в двумерном массиве
    a[i][j], i — индекс строки).*/
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            A = 0;
            if ((DayNow % N) == i) // Определение нужной строки для суммирования
            {
                for (j = 0; j < N; j++)
                {
                    A=A+Array[i][j];
                }
                cout << "Сумма строки под индексом " << i << " равна - "<<A << endl;
            }
        }
    }
    return 0;
}